tg_no_deleted_messages() { grep -v -e '"flags":272,' -e '"flags":258,' ; }
tg_no_service_messages() { grep -v -e '"flags":8450' ; }

(tg-run-command.sh history   $1 $2 |grep participa |jq -c .[]|grep -v SUCCESS|tr -d '\n'|sed 's/{"event/\n{"event/g')|tg_no_service_messages|tg_no_deleted_messages|while read line;do
# echo "$line"

msgid=$(echo "$line" |jq .id|cut -d'"' -f2)
msgtxt=$(echo "$line" |jq .id 2>/dev/null|cut -d'"' -f2 2>/dev/null;echo "$line" |jq .id 2>/dev/null|cut -d'"' -f2 2>/dev/null)
[[ -z "$msgid" ]] && [[ -z "$msgtxt" ]] ||  {
echo -n DEDUP_ID: $msgid
mysum=$(( echo $line|jq .text|sha1sum;echo $line|jq .caption|sha1sum ) |tr -d '\n' )
( test -e /dev/shm/tg.seenlines.$1 && cat /dev/shm/tg.seenlines.$1 |grep "$mysum"  -q &&  ( echo "purge : $msgid" >&2 ;tg-run-command.sh delete_msg $msgid |tr -d '\n') ) &
( test -e /dev/shm/tg.seenlines.$1 && cat /dev/shm/tg.seenlines.$1 |grep "$mysum"  -q ||  ( echo "nopurg: $msgid" ;echo "$mysum">>/dev/shm/tg.seenlines.$1;echo ) ) &
echo -n ; }
done 

test -e /dev/shm/tg.seenlines.$1 && rm /dev/shm/tg.seenlines.$1
#|while read cmd;do tg-console.sh|sed 's/ANSWER/|ANSWER|/g' |tr -d '\n';done;echo 

#-e '"text":"/info ' -e groupanonymousbot -e "Your ID: 1087968824" |jq .id|cut -d '"' -f2 |sed 's/^/delete_msg /g'
