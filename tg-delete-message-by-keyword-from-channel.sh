
KEYWORD=$3
[[ -z "$KEYWORD" ]] && { echo "NO KEYWORD";exit 13 ; } ;

tg-get-channel-history.sh $1 $2|grep -e $KEYWORD -e 'MigrateToChat: The group has been migrated to a supergroup'|jq .id|cut -d '"' -f2 |sed 's/^/delete_msg /g'|while read cmd;do 
tg-console.sh|sed 's/ANSWER/|ANSWER|/g' |tr -d '\n';done;echo 
